DROP TABLE Editeurs CASCADE CONSTRAINT;
DROP TABLE Auteurs CASCADE CONSTRAINT;
DROP TABLE Livres CASCADE CONSTRAINT;
DROP TABLE Ecrire CASCADE CONSTRAINT;

-- creation des tables

CREATE TABLE Editeurs
(nomEditeur VARCHAR(25), telEditeur VARCHAR(20),
CONSTRAINT pk_Editeurs PRIMARY KEY (nomEditeur)) ;

CREATE TABLE Auteurs
(numAuteur VARCHAR(5), nomAuteur VARCHAR(25), prenomAuteur VARCHAR(25), anneeNaissance NUMBER,
CONSTRAINT pk_Auteurs PRIMARY KEY (numAuteur)) ;

CREATE TABLE Livres
(numLivre VARCHAR(5), nomLivre VARCHAR(50), sujet VARCHAR(25), prix NUMBER, anneeEdition NUMBER, qteStock NUMBER, nomEditeur VARCHAR(25),
CONSTRAINT pk_Livres PRIMARY KEY (numLivre),
CONSTRAINT fk_Livres_nomEditeur FOREIGN KEY (nomEditeur) REFERENCES Editeurs(nomEditeur),
CONSTRAINT u_Livres_nomLivre UNIQUE (nomLivre));

CREATE TABLE Ecrire
(numLivre VARCHAR(5), numAuteur VARCHAR(5),
CONSTRAINT pk_Ecrire PRIMARY KEY (numLivre, numAuteur),
CONSTRAINT fk_Ecrire_numLivre FOREIGN KEY (numLivre) REFERENCES Livres(numLivre),
CONSTRAINT fk_Ecrire_numAuteur FOREIGN KEY (numAuteur) REFERENCES Auteurs(numAuteur));


--insertion de donn�es dans les tables

INSERT INTO Editeurs (nomEditeur, telEditeur) (SELECT * FROM Pallejan.BIBLIO_Editeurs);
INSERT INTO Auteurs (numAuteur, nomAuteur, prenomAuteur, anneeNaissance) (SELECT * FROM Pallejan.BIBLIO_Auteurs);
INSERT INTO Livres (numLivre, nomLivre, sujet, prix, anneeEdition, qteStock, nomEditeur) (SELECT * FROM Pallejan.BIBLIO_Livres);
INSERT INTO Ecrire (numLivre, numAuteur) (SELECT * FROM Pallejan.BIBLIO_Ecrire);

COMMIT;
