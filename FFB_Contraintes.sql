SET SERVEROUTPUT ON;
SET AUTOCOMMIT OFF;

DECLARE
	ERREUR_STRUCTURE EXCEPTION;
	TABLE_NOT_EXISTS EXCEPTION;
	PRAGMA EXCEPTION_INIT(TABLE_NOT_EXISTS,-00942);
	NB_ERREURS NUMBER ;

BEGIN
	COMMIT;
	NB_ERREURS := 0;
	DBMS_OUTPUT.PUT_LINE('Lancement du programme de test') ;
	
	BEGIN
		EXECUTE IMMEDIATE 'SELECT * FROM Clubs';
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (''c1'', ''La bille rouge'', ''Montpellier'')';
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (''c2'', ''La bille bleue'', ''Montpellier'')';
	EXCEPTION
		WHEN TABLE_NOT_EXISTS THEN
			DBMS_OUTPUT.PUT_LINE('La table Clubs n''existe pas') ;
			RAISE ERREUR_STRUCTURE ;
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur sur la structure de la table Clubs') ;
			RAISE ERREUR_STRUCTURE ;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'SELECT * FROM Joueurs';
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub) VALUES (''j1'', ''Ouzy'', ''Jacques'', ''M'', ''c1'')';
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j2'', ''Nemard'', ''Jean'', ''M'', ''c1'', ''j1'')';	
	EXCEPTION
		WHEN TABLE_NOT_EXISTS THEN
			DBMS_OUTPUT.PUT_LINE('La table Joueurs n''existe pas') ;
			RAISE ERREUR_STRUCTURE ;
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur sur la structure de la table Joueurs') ;
			RAISE ERREUR_STRUCTURE ;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'SELECT * FROM Tournois';
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t1'', ''Billes en f�te'', ''Montpellier'', 800, ''c1'')';
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t2'', ''Les billes vendangent'', ''Montpellier'', 2000, ''c2'')';
	EXCEPTION
		WHEN TABLE_NOT_EXISTS THEN
			DBMS_OUTPUT.PUT_LINE('La table Tournois n''existe pas') ;
			RAISE ERREUR_STRUCTURE ;
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur sur la structure de la table Tournois') ;
			RAISE ERREUR_STRUCTURE ;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'SELECT * FROM Participer';
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t1'', ''j1'', 2)';
	EXCEPTION
		WHEN TABLE_NOT_EXISTS THEN
			DBMS_OUTPUT.PUT_LINE('La table Participer n''existe pas') ;
			RAISE ERREUR_STRUCTURE ;
		WHEN OTHERS THEN
			DBMS_OUTPUT.PUT_LINE('Erreur sur la structure de la table Participer') ;
			RAISE ERREUR_STRUCTURE ;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (''c1'', ''La bille rouge'', ''Montpellier'')';
		DBMS_OUTPUT.PUT_LINE('Erreur de doublons sur cl� primaire table Clubs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (NULL, ''La bille rouge'', ''Montpellier'')';
		DBMS_OUTPUT.PUT_LINE('Erreur avec cl� primaire nulle table Clubs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub) VALUES (''j1'', ''Ouzy'', ''Jacques'', ''M'', ''c1'')';
		DBMS_OUTPUT.PUT_LINE('Erreur de doublons sur cl� primaire table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub) VALUES (NULL, ''Ouzy'', ''Jacques'', ''M'', ''c1'')';
		DBMS_OUTPUT.PUT_LINE('Erreur avec cl� primaire nulle table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t1'', ''Billes en f�te'', ''Montpellier'', 800, ''c1'')';
		DBMS_OUTPUT.PUT_LINE('Erreur de doublons sur cl� primaire table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (NULL, ''Billes en f�te'', ''Montpellier'', 800, ''c1'')';
		DBMS_OUTPUT.PUT_LINE('Erreur avec cl� primaire nulle table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t1'', ''j1'', 2)';
		DBMS_OUTPUT.PUT_LINE('Erreur de doublons sur cl� primaire table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (NULL, ''j1'', 2)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec cl� primaire nulle table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t1'', NULL, 2)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec cl� primaire nulle table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub) VALUES (''j3'', ''Ouzy'', ''Jacques'', ''M'', ''c3'')';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Contrainte d''Int�grit� R�f�rentielle de codeClub de la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j4'', ''Nemard'', ''Jean'', ''M'', ''c1'', ''j9'')';	
		DBMS_OUTPUT.PUT_LINE('Erreur avec Contrainte d''Int�grit� R�f�rentielle de codeJoueurParrain de la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t3'', ''Billes en f�te'', ''Montpellier'', 800, ''c4'')';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Contrainte d''Int�grit� R�f�rentielle de codeClubOrganisateur de la table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;

	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t6'', ''j1'', 2)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Contrainte d''Int�grit� R�f�rentielle de codeTournoi de la table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t1'', ''j6'', 2)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Contrainte d''Int�grit� R�f�rentielle de codeJoueur de la table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (''c12'', ''Club12'', NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec format de codeClub de la table Clubs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j11'', ''Nemard'', ''Jean'', ''M'', ''c1'', NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec format de codeJoueur de la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t11'', ''Les billes'', ''Montpellier'', 100, NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec format de codeTournoi de la table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Clubs (codeClub, nomClub, villeClub) VALUES (''c3'', NULL, ''Montpellier'');';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Clubs qui peuvent ne pas avoir de nomClub dans la table Clubs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;

	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j4'', NULL, ''Jean'', ''M'', ''c1'', NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Joueurs qui peuvent ne pas avoir de nomJoueur dans la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;

	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j5'', ''Nemard'', NULL, ''M'', ''c1'', NULL)';
	EXCEPTION
		WHEN OTHERS THEN 
			DBMS_OUTPUT.PUT_LINE('Erreur avec Joueurs qui ne peut pas ne pas avoir de prenom dans la table Joueurs') ;
			NB_ERREURS := NB_ERREURS + 1 ;
	END ;

	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j6'', ''Nemard'', ''Jean'', ''M'', NULL, NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Joueurs qui peuvent ne pas avoir de codeClub dans la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j7'', ''Nemard'', ''Jean'', ''B'', ''c1'', NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec valeurs possibles de sexeJoueur de la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;

	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Joueurs (codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain) VALUES (''j8'', ''Nemard'', ''Jean'', NULL, ''c1'', NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Joueurs qui peuvent ne pas avoir de sexe dans la table Joueurs') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t3'', NULL, ''Montpellier'', 100, NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Tournois qui peuvent ne pas avoir de nomTournoi dans la table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t4'', ''Les Billes'', NULL, 100, NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec Tournois qui peuvent ne pas avoir de villeTournoi dans la table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;

	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Tournois (codeTournoi, nomTournoi, villeTournoi, totalPrixDistribues, codeClubOrganisateur) VALUES (''t5'', ''Les Billes'', ''Montpellier'', -1, NULL)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec totalPrixDistribues qui peut �tre n�gatif dans la table Tournois') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t2'', ''j1'', -2)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec classement qui peut �tre n�gatif dans la table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t1'', ''j2'', 0)';
		DBMS_OUTPUT.PUT_LINE('Erreur avec classement qui peut �tre inf�rieur � 1 dans la table Participer') ;
		NB_ERREURS := NB_ERREURS + 1 ;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;
	
	
	DECLARE
		v_classement NUMBER;
	BEGIN
		EXECUTE IMMEDIATE 'INSERT INTO Participer (codeTournoi, codeJoueur, classement) VALUES (''t2'', ''j2'', 2.3)';
		EXECUTE IMMEDIATE 'SELECT classement FROM Participer WHERE codeTournoi = ''t2'' AND codeJoueur = ''j2''' INTO v_classement ;
		IF v_classement <> 2 THEN
			DBMS_OUTPUT.PUT_LINE('Erreur avec classement qui peut ne pas �tre entier dans la table Participer') ;
			NB_ERREURS := NB_ERREURS + 1 ;
		END IF;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END ;


	IF NB_ERREURS = 0
	THEN
		DBMS_OUTPUT.PUT_LINE('Pas d''erreur ; tout va bien. Vous pouvez passer � la question suivante') ;
	ELSE
		DBMS_OUTPUT.PUT_LINE('Il y a ' || NB_ERREURS || ' erreur(s). Corrigez les avant de passer � la suite') ;
	END IF ;
	ROLLBACK ;



EXCEPTION
	WHEN ERREUR_STRUCTURE THEN
		DBMS_OUTPUT.PUT_LINE('Il y a une erreur sur la structure d''une table. V�rification du respect des contraintes impossible') ;
		ROLLBACK ;
	WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE('Echec du programme de Test') ;
		ROLLBACK ;

END ;