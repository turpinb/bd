-- TD9 1 2)
drop table JOUEURS cascade constraint;
drop table CLUBS cascade constraint;
drop table TOURNOIS cascade constraint;
drop table PARTICIPER cascade constraint;


CREATE table CLUBS
(
    codeClub  char(2),
    nomClub   varchar(40),
    villeClub varchar(40),
    constraint pk_Clubs primary key (codeClub),
    constraint nn_nomClub CHECK ( nomClub is not null )
);
Create table JOUEURS
(
    codeJoueur        char(2),
    nomJoueur         varchar(40),
    prenomJoueur      varchar(40),
    sexeJoueur        char(1),
    codeClub          char(2),
    codeJoueurParrain char(2),
    constraint pk_Joueurs primary key (codeJoueur),
    constraint fk_CodeClub_Joueurs foreign key (codeClub) references CLUBS (codeClub) on delete cascade,
    constraint nn_codeClub CHECK ( codeClub is not null ),
    constraint fk_codeParain foreign key (codeJoueurParrain) references JOUEURS (codeJoueur),
    constraint ck_valeursSexe CHECK ( sexeJoueur = 'M' or sexeJoueur = 'F' ),
    constraint nn_sexe CHECK ( sexeJoueur is not null ),
    constraint nn_nomJoueur check ( nomJoueur is not null )
);
create table TOURNOIS
(
    codeTournoi          char(2),
    nomTournoi           varchar(40),
    villeTournoi         varchar(30),
    totalPrixDistribues  number,
    codeClubOrganisateur char(2),
    constraint pk_tournoi primary key (codeTournoi),
    constraint fk_cluborga foreign key (codeClubOrganisateur) references CLUBS (codeClub),
    constraint nn_nomTournoi_ville check ( nomTournoi is not null and villeTournoi is not null),
    constraint val_totalPrix check ( totalPrixDistribues >= 0 )
);
create table PARTICIPER
(
    codeTournoi char(2),
    codeJoueur  char(2),
    classement  number(38),
    constraint pk_participer primary key (codeTournoi, codeJoueur),
    constraint fk_codetn foreign key (codeTournoi) references TOURNOIS (codeTournoi),
    constraint fk_codejr foreign key (codeJoueur) references JOUEURS (codejoueur),
    constraint val_classement check ( classement >= 1 )
);

-- 3)

INSERT into CLUBS(codeClub, nomClub, villeClub)
values ('C1', 'La bille noire', 'Montpellier');
INSERT into CLUBS(codeClub, nomClub, villeClub)
values ('C2', 'La bille blanche', 'Montpellier');
INSERT into CLUBS(codeClub, nomClub, villeClub)
values ('C3', 'CrocoBilles', 'Nimes');
INSERT into CLUBS(codeClub, nomClub, villeClub)
values ('C4', 'La billeroise', 'Béziers');
INSERT into CLUBS(codeClub, nomClub, villeClub)
values ('C5', 'La billamende', 'Mende');

INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub)
values ('J1', 'Ouzy', 'Jacques', 'M', 'C1');
INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub)
values ('J5', 'Pique', 'Sam', 'M', 'C2');
INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain)
values ('J2', 'Bricot', 'Judas', 'M', 'C2', 'J5');
INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain)
values ('J3', 'Nemard', 'Jean', 'M', 'C1', 'J1');
INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain)
values ('J4', 'Zeblouze', 'Agathe', 'F', 'C1', 'J1');

INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub)
values ('J6', 'Bricot', 'Judas', 'M', 'C3');
INSERT into JOUEURS(codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub)
values ('J7', 'Nanas', 'Judas', 'M', 'C5');

INSERT into TOURNOIS(codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur)
values ('T1', 'Billes en fête', 'Montpellier', 800, 'C1');
INSERT into TOURNOIS(codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur)
values ('T2', 'Le feu d''artifice de billes', 'Montpellier', 1550, 'C1');
INSERT into TOURNOIS(codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur)
values ('T3', 'Les crocodiles mangent des billes', 'Nimes', 2500, 'C3');
INSERT into TOURNOIS(codetournoi, nomtournoi, villetournoi, totalprixdistribues)
values ('T4', 'Les billes en joute', 'Sète', 1200);
INSERT into TOURNOIS(codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur)
values ('T5', 'Les billes vendangent', 'Montpellier', 2000, 'C2');

INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T1', 'J1', 2);
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T1', 'J2', 1);
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T1', 'J3', 3);
INSERT into PARTICIPER(codeTournoi, codeJoueur)
values ('T1', 'J6');
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T3', 'J1', 1);
INSERT into PARTICIPER(codeTournoi, codeJoueur)
values ('T3', 'J4');
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T4', 'J1', 3);
INSERT into PARTICIPER(codeTournoi, codeJoueur)
values ('T4', 'J2');
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T4', 'J4', 1);
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T4', 'J6', 2);
INSERT into PARTICIPER(codeTournoi, codeJoueur, classement)
values ('T5', 'J1', 1);

-- 4)

ALTER TABLE JOUEURS add (dateJoueur date);

Update JOUEURS
set dateJoueur = '11/03/1982'
where codeJoueur = 'J1';

Update JOUEURS
set dateJoueur = '25/12/1982'
where codeJoueur = 'J2'
   or codeJoueur = 'J4';

Update JOUEURS
set dateJoueur = '02/09/1982'
where codeJoueur = 'J3';

Update JOUEURS
set dateJoueur = '25/12/1979'
where codeJoueur = 'J5';

-- 5)
Update PARTICIPER
set classement= 2
where codeJoueur = 'J4'
  and codeTournoi = 'T3';

-- 6)
Delete
from CLUBS
where codeClub = 'C5';

-- 8)

-- R16

select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in (select j1.codeJoueur
                     from JOUEURS j1
                              join JOUEURS j2 on j1.dateJoueur = j2.dateJoueur and j1.codeJoueur <> j2.codeJoueur);


-- -------------------------------------------------------------------------------------------------

-- TD11
-- R1
select codeClub, count(codeJoueur) as NBJOUEURS
from JOUEURS
group by codeClub;

-- R2
select villeTournoi, sum(totalPrixDistribues) as SOMMEPRIX
from TOURNOIS
group by villeTournoi
order by SOMMEPRIX desc;

-- R3
select T.nomTournoi, count(codeJoueur) as NBPARTICIPANTS
from TOURNOIS T
         join PARTICIPER p on p.codeTournoi = T.codeTournoi
group by T.codeTournoi, T.nomTournoi;

-- R4
select nomClub, min(dateJoueur) as DATENAISSANCE
from JOUEURS
         join CLUBS C2 on C2.codeClub = JOUEURS.codeClub
group by nomClub;
order by DATENAISSANCE;

-- R5
select C.nomClub, avg(totalPrixDistribues) as PRIXMOYEN
from TOURNOIS t
join CLUBS C on t.codeClubOrganisateur = C.codeClub
where C.villeClub = 'Montpellier'
group by C.nomClub;

-- R6
select j.nomJoueur, j.prenomJoueur, count(p.codeJoueur) as NBTOURNOIS
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
group by j.nomJoueur, j.prenomJoueur, j.codeJoueur;

-- R7
select parrain.nomJoueur, parrain.prenomJoueur, count(filleuls.codeJoueur) as NBFILLEULS
from JOUEURS filleuls
join JOUEURS parrain on filleuls.codeJoueurParrain = parrain.codeJoueur
group by parrain.nomJoueur, parrain.prenomJoueur, parrain.codeJoueurParrain;

-- R8
select t.nomTournoi, count(p.codeJoueur) as NBJOUEURS
from PARTICIPER p
join TOURNOIS t on p.codeTournoi = t.codeTournoi
join JOUEURS j on p.codeJoueur = j.codeJoueur
where j.sexeJoueur = 'M'
group by t.nomTournoi;

-- R9
select t.nomTournoi, count(distinct J.codeClub) as NBCLUBS
from TOURNOIS t
join PARTICIPER P on t.codeTournoi = P.codeTournoi
JOIN JOUEURS J on P.codeJoueur = J.codeJoueur
group by t.nomTournoi;

-- R10
SELECT C.codeClub, COUNT(J.codeJoueur) AS NBJoueurs
FROM CLUBS C
         LEFT JOIN JOUEURS J ON C.codeClub = J.codeClub
GROUP BY C.codeClub;

-- R11
select count(codeTournoi) as NBTOURNOIS
from PARTICIPER
where codeJoueur = 'J1';

-- R12
select villeClub, count(codeClub)
from CLUBS
group by villeClub;

-- R13
select count(codeTournoi) as NBTOURNOIS
from TOURNOIS
where codeClubOrganisateur = 'C2';

-- R14
select nomTournoi
from TOURNOIS t
join PARTICIPER P on t.codeTournoi = P.codeTournoi
where P.codeJoueur = 'J1'
INTERSECT
select nomTournoi
from TOURNOIS t
         join PARTICIPER P on t.codeTournoi = P.codeTournoi
where P.codeJoueur = 'J2';

-- R15
select count(DISTINCT villeTournoi) as NBVILLES
from TOURNOIS
where villeTournoi is not null;

-- R16
select villeTournoi
from TOURNOIS
group by villeTournoi
order by avg(totalPrixDistribues) desc;

-- R17
select nomJoueur, prenomJoueur
from JOUEURS
where dateJoueur in(
    select max(dateJoueur)
    from JOUEURS
    );

-- R18
select nomClub
from CLUBS
where codeClub in(
    select codeClub
    from JOUEURS
    minus
    select codeClub
    from JOUEURS
    where sexeJoueur = 'F'
    );

-- R19
select nomClub, count(codeJoueur)
from JOUEURS j
join CLUBS c on j.codeClub = c.codeClub
where sexeJoueur = 'M'
group by nomClub;

-- R20
select DISTINCT nomTournoi
from TOURNOIS
join PARTICIPER P on TOURNOIS.codeTournoi = P.codeTournoi
join PARTICIPER P2 on P.codeTournoi = P2.codeTournoi and P.codeJoueur <> P2.codeJoueur;

-- R21
select nomTournoi
from TOURNOIS
where villeTournoi = 'Montpellier'
and totalPrixDistribues = (select min(totalPrixDistribues)
                           from TOURNOIS
                           where villeTournoi = 'Montpellier');

-- R22
select nomJoueur, prenomJoueur
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
minus
select nomJoueur, prenomJoueur
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
join TOURNOIS T on P.codeTournoi = T.codeTournoi
where totalPrixDistribues < 1000;

-- R23
select c.codeClub, nomClub, count(T.codeTournoi)
from CLUBS c
left join TOURNOIS T on c.codeClub = T.codeClubOrganisateur
group by c.codeClub, nomClub;

-- R24
select DISTINCT j.codeJoueur, nomJoueur, prenomJoueur
from  JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
join PARTICIPER PJ4 on P.codeTournoi = PJ4.codeTournoi
where PJ4.codeJoueur = 'J4'
minus
select codeJoueur, nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur = 'J4';

-- R25
select count(filleuls.codeJoueur) as NBFILLEULS
from JOUEURS filleuls
         join JOUEURS parrain on filleuls.codeJoueurParrain = parrain.codeJoueur
where parrain.prenomJoueur = 'Jacques' and parrain.nomJoueur = 'Ouzy'
group by parrain.codeJoueur;

-- R26
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in(
    select codeJoueur
    from JOUEURS j
    join CLUBS C on j.codeClub = C.codeClub
    where C.villeClub = 'Montpellier'
    union
    select codeJoueur
    from PARTICIPER p
    join TOURNOIS T on p.codeTournoi = T.codeTournoi
    where T.villeTournoi = 'Montpellier'
    )
order by nomJoueur, prenomJoueur;

-- R27
select j.codeJoueur, nomJoueur, prenomJoueur, count(codeTournoi) as NBTOURNOIS
from JOUEURS j
left join PARTICIPER P on j.codeJoueur = P.codeJoueur
group by j.codeJoueur, nomJoueur, prenomJoueur;

-- R28
select nomJoueur, prenomJoueur
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
join TOURNOIS T on P.codeTournoi = T.codeTournoi
where T.nomTournoi = 'Les crocodiles mangent des billes'
and P.classement = (
    select max(classement)
    from PARTICIPER P
    join TOURNOIS T on P.codeTournoi = T.codeTournoi
    where T.nomTournoi = 'Les crocodiles mangent des billes'
    ) ;

-- R29
select j1.nomJoueur, j1.prenomJoueur
from JOUEURS j1
join JOUEURS j2 on j1.codeJoueur <> j2.codeJoueur and j1.dateJoueur = j2.dateJoueur;

-- R30
select nomJoueur
from JOUEURS
where codeJoueur in (select codeJoueurParrain
                    from JOUEURS
                    where dateJoueur in(
                        select min(parrain.dateJoueur)
                        from JOUEURS j
                        join JOUEURS parrain on j.codeJoueur = parrain.codeJoueurParrain
                        ));

-- R31
select villeClub, count(codeJoueur)
from JOUEURS j
         join CLUBS C2 on j.codeClub = C2.codeClub
group by villeClub;