-- TD9 1 2)
drop table JOUEURS cascade constraint;
drop table CLUBS cascade constraint;
drop table TOURNOIS cascade constraint;
drop table PARTICIPER cascade constraint;


CREATE table CLUBS(
                      codeClub char(2), nomClub varchar(40), villeClub varchar(40),
                      constraint pk_Clubs primary key (codeClub),
                      constraint nn_nomClub CHECK ( nomClub is not null )
);
Create table JOUEURS(
                        codeJoueur char(2), nomJoueur varchar(40), prenomJoueur varchar(40), sexeJoueur char(1), codeClub char(2), codeJoueurParrain char(2),
                        constraint pk_Joueurs primary key (codeJoueur),
                        constraint fk_CodeClub_Joueurs foreign key (codeClub) references CLUBS(codeClub) on delete cascade,
                        constraint nn_codeClub CHECK ( codeClub is not null ),
                        constraint fk_codeParain foreign key (codeJoueurParrain) references JOUEURS(codeJoueur),
                        constraint ck_valeursSexe CHECK ( sexeJoueur = 'M' or sexeJoueur = 'F' ),
                        constraint nn_sexe CHECK ( sexeJoueur is not null ),
                        constraint nn_nomJoueur check ( nomJoueur is not null )
);
create table TOURNOIS(
                         codeTournoi char(2), nomTournoi varchar(40), villeTournoi varchar(30), totalPrixDistribues number, codeClubOrganisateur char(2),
                         constraint pk_tournoi primary key (codeTournoi),
                         constraint fk_cluborga foreign key (codeClubOrganisateur) references CLUBS(codeClub),
                         constraint nn_nomTournoi_ville check ( nomTournoi is not null and villeTournoi is not null),
                         constraint val_totalPrix check ( totalPrixDistribues >= 0 )
);
create table PARTICIPER(
                           codeTournoi char(2), codeJoueur char(2), classement number(38),
                           constraint pk_participer primary key (codeTournoi, codeJoueur),
                           constraint fk_codetn foreign key (codeTournoi) references TOURNOIS(codeTournoi),
                           constraint fk_codejr foreign key (codeJoueur) references JOUEURS(codejoueur),
                           constraint val_classement check ( classement >= 1 )
);

-- 3)

INSERT into CLUBS(
  codeClub, nomClub, villeClub
  ) values ('C1','La bille noire', 'Montpellier'

);
INSERT into CLUBS(
    codeClub, nomClub, villeClub
) values ('C2','La bille blanche','Montpellier'

);
INSERT into CLUBS(
    codeClub, nomClub, villeClub
) values ('C3','CrocoBilles','Nimes'

);
INSERT into CLUBS(
    codeClub, nomClub, villeClub
) values ('C4','La billeroise','Béziers'

);
INSERT into CLUBS(
    codeClub, nomClub, villeClub
) values ('C5','La billamende','Mende'

);

INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub
) values ('J1', 'Ouzy', 'Jacques', 'M', 'C1'

         );
INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub
) values ('J5', 'Pique', 'Sam', 'M', 'C2'

         );
INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain
) values ('J2', 'Bricot', 'Judas', 'M', 'C2', 'J5'

         );
INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain
) values ('J3', 'Nemard', 'Jean', 'M', 'C1', 'J1'

         );
INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub, codeJoueurParrain
) values ('J4', 'Zeblouze', 'Agathe', 'F', 'C1', 'J1'

         );

INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub
) values ('J6', 'Bricot', 'Judas', 'M', 'C3'

         );
INSERT into JOUEURS(
    codeJoueur, nomJoueur, prenomJoueur, sexeJoueur, codeClub
) values ('J7', 'Nanas', 'Judas', 'M', 'C5'

         );

INSERT into TOURNOIS(
    codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur
) values ('T1', 'Billes en fête', 'Montpellier', 800, 'C1'

         );
INSERT into TOURNOIS(
    codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur
) values ('T2', 'Le feu d''artifice de billes', 'Montpellier', 1550, 'C1'

         );
INSERT into TOURNOIS(
    codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur
) values ('T3', 'Les crocodiles mangent des billes', 'Nimes', 2500, 'C3'

         );
INSERT into TOURNOIS(
    codetournoi, nomtournoi, villetournoi, totalprixdistribues
) values ('T4', 'Les billes en joute', 'Sète', 1200

         );
INSERT into TOURNOIS(
    codetournoi, nomtournoi, villetournoi, totalprixdistribues, codecluborganisateur
) values ('T5', 'Les billes vendangent', 'Montpellier', 2000, 'C2'

         );

INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T1','J1',2

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T1','J2',1

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T1','J3',3

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur
) values ('T1','J6'

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T3','J1',1

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur
) values ('T3','J4'

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T4','J1',3

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur
) values ('T4','J2'

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T4','J4',1

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T4','J6',2

         );
INSERT into PARTICIPER(
    codeTournoi, codeJoueur, classement
) values ('T5','J1',1

         );

-- 4)

ALTER TABLE JOUEURS add (dateJoueur date);

Update JOUEURS set dateJoueur = '11/03/1982'
where codeJoueur = 'J1';

Update JOUEURS set dateJoueur = '25/12/1982'
where codeJoueur = 'J2' or codeJoueur = 'J4';

Update JOUEURS set dateJoueur = '02/09/1982'
where codeJoueur = 'J3';

Update JOUEURS set dateJoueur = '25/12/1979'
where codeJoueur = 'J5';

-- 5)
Update PARTICIPER set classement= 2
where codeJoueur = 'J4' and codeTournoi = 'T3';

-- 6)
Delete from CLUBS
where codeClub='C5';

-- 8)

-- R16

select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in(select j1.codeJoueur
                    from JOUEURS j1
                    join JOUEURS j2 on j1.dateJoueur = j2.dateJoueur and j1.codeJoueur <> j2.codeJoueur);


-- -------------------------------------------------------------------------------------------------

-- TD10
-- R1
select count(*) as NBTOURNOIS
from TOURNOIS;

-- R2
select sum(totalPrixDistribues) as SOMME, avg(totalPrixDistribues) as MOYENNE, min(totalPrixDistribues) as MINIMUM, max(totalPrixDistribues) as MANIMUM
from TOURNOIS;

-- R3
select count(codeClub) as NBJOUEURS
from JOUEURS
where codeClub = 'C1';

-- R4
select max(dateJoueur) as  DATEJEUNE
from JOUEURS
where prenomJoueur like 'J%';

-- R6
select min(classement) as MEILLEURCLASSEMENT
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
where j.prenomJoueur = 'Jacques' and j.nomJoueur = 'Ouzy';

-- R7
select sum(totalPrixDistribues) as SOMMEPRIX
from TOURNOIS t
join CLUBS C on t.codeClubOrganisateur = C.codeClub
where C.nomClub = 'La bille noire';

-- R8
select count(*) as NBJOUEURS
from PARTICIPER p
join TOURNOIS T on p.codeTournoi = T.codeTournoi
where T.codeClubOrganisateur in (select codeClub
                                 from CLUBS
                                 where nomClub = 'CrocoBilles');

-- R9
select count(distinct codeJoueur) as NBJOUEURS
from PARTICIPER;

-- R10
select nomTournoi
from TOURNOIS
where totalPrixDistribues = (select max(totalPrixDistribues)
                             from TOURNOIS);

-- R11
select nomJoueur
from JOUEURS
where codeClub = 'C1'
and dateJoueur = (select min(dateJoueur)
                  from JOUEURS
                  where codeClub = 'C1');

-- R12
select count(*) as NBJOUEURS
from JOUEURS
where codeJoueur in (SELECT codeJoueur
                     from JOUEURS
                     minus
                     select codeJoueur
                     from PARTICIPER);

-- R13
select count(distinct codeTournoi) as NBTOURNOIS
from TOURNOIS
where codeTournoi in (select codeTournoi
                      from PARTICIPER
                      where codeJoueur = 'J1'
                      intersect
                      select codeTournoi
                      from PARTICIPER
                      where codeJoueur = 'J2');

-- R14
select distinct j.codeJoueur, nomJoueur, prenomJoueur
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
where codeClub = 'C2';

-- R15
select nomJoueur
from JOUEURS
where codeJoueur in (select j.codeJoueur
                     from JOUEURS j
                     join PARTICIPER p on j.codeJoueur = p.codeJoueur
                     join TOURNOIS T on p.codeTournoi = T.codeTournoi
                     where T.villeTournoi = 'Montpellier');

-- R16
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in (select codeJoueurParrain
                     from JOUEURS
                     where nomJoueur = 'Nemard' and prenomJoueur = 'Jean');

-- R17
select avg(totalPrixDistribues) as TOTALMOYEN
from TOURNOIS
where villeTournoi = 'Montpellier';

-- R18
select nomTournoi
from TOURNOIS
where nomTournoi in(select t.nomTournoi
                    from TOURNOIS t
                             join TOURNOIS t1 on t.villeTournoi = t1.villeTournoi
                    where t1.codeTournoi = 'T1'
    minus
select nomTournoi
from TOURNOIS
where codeTournoi = 'T1')
order by totalPrixDistribues desc;

-- R19
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in(select codeJoueur
                    from PARTICIPER
                    where codeTournoi = 'T1'
                    intersect
                    select codeJoueur
                    from PARTICIPER
                    where codeTournoi = 'T4');

-- R20
select count(*) as PARTICIPANTS
from PARTICIPER p
join TOURNOIS T on p.codeTournoi = T.codeTournoi
where T.nomTournoi = 'Billes en fête';

-- R21
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueurParrain in(select codeJoueur
                           from JOUEURS
                           where nomJoueur = 'Ouzy' and prenomJoueur = 'Jacques');

-- R22
select nomTournoi
from TOURNOIS
where codeTournoi in (select codeTournoi
                      from TOURNOIS
                      minus
                      select codeTournoi
                      from PARTICIPER);

-- R23
select count(distinct codeClubOrganisateur) as NBCLUBS
from TOURNOIS;

-- R24
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueurParrain is null;

-- R25
select codeJoueur, nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in(select p1.codeJoueur
                    from PARTICIPER p1
                    join PARTICIPER p2 on p1.codeJoueur = p2.codeJoueur and p1.codeTournoi <> p2.codeTournoi);

-- R26
select codeJoueur, nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in(select j.codeJoueur
                    from JOUEURS j
                    join PARTICIPER p on j.codeJoueur = p.codeJoueur
                    join TOURNOIS T on p.codeTournoi = T.codeTournoi and t.codeClubOrganisateur = j.codeClub);
-- 27
select nomTournoi
from TOURNOIS
where codeTournoi in (select p.codeTournoi
                      from PARTICIPER p
                      join JOUEURS J on p.codeJoueur = J.codeJoueur
                      join TOURNOIS T on p.codeTournoi = T.codeTournoi
                      where j.nomJoueur = 'Ouzy' and j.prenomJoueur = 'Jacques'
                      and T.totalPrixDistribues = (select MAX(totalPrixDistribues)
                                                   from TOURNOIS));

-- R28
select codeJoueur
from PARTICIPER
minus
select j.codeJoueur
from JOUEURS j
join PARTICIPER P on j.codeJoueur = P.codeJoueur
join TOURNOIS T on P.codeTournoi = T.codeTournoi
where j.codeClub != T.codeClubOrganisateur;

-- R29
select nomJoueur, prenomJoueur
from JOUEURS
where codeJoueur in (select codeJoueur
                     from PARTICIPER
                     minus
                     select codeJoueur
                     from PARTICIPER p
                     join TOURNOIS T on p.codeTournoi = T.codeTournoi
                     where T.villeTournoi != 'Montpellier');

-- R30
SELECT DISTINCT J.codeJoueur, J.nomJoueur, J.prenomJoueur
FROM JOUEURS J
JOIN PARTICIPER P ON J.codeJoueur = P.codeJoueur
JOIN PARTICIPER PP ON J.codeJoueurParrain = PP.codeJoueur and P.codeTournoi = PP.codeTournoi;









