-- ex 9
-- R130
select e.nomEmploye, e.prenomEmploye, chef.nomEmploye
from EMPLOYES e
left join EMPLOYES chef on e.idEmployeChef = chef.idEmploye
where e.idCamping is null
order by e.nomEmploye;

-- R131
-- ne marche pas
select c.nomClient, c.prenomClient
from  CLIENTS c
where not exists(select b.idLocation
                 from LOCATIONS b
                 join BUNGALOWS bu on bu.idBungalow = b.idBungalow
                 join CAMPINGS ca on ca.idCamping = bu.idCamping
                 where ca.nbEtoilesCamping=5
                 minus
                 select l.idLocation
                 from LOCATIONS l
                 where c.idClient = l.idClient);

-- bis fonctionnelle
select c.nomClient, c.prenomClient
from CLIENTS c
join LOCATIONS l on l.idClient = c.idClient
join BUNGALOWS b on b.idBungalow = l.idBungalow
join CAMPINGS ca on ca.idCamping = b.idCamping
where ca.nbEtoilesCamping=5
group by c.nomClient, c.prenomClient, c.idClient
having count(distinct ca.idCamping) = (select count(*)
                   from CAMPINGS
                   where nbEtoilesCamping=5);

-- R132
select c.nomClient, c.prenomClient
from CLIENTS c
where idClient in(select idClient
                    from LOCATIONS
                    minus
                    select idClient
                    from LOCATIONS l
                    join BUNGALOWS b on l.idBungalow = b.idBungalow
                    join CAMPINGS ca on ca.idCamping = b.idCamping
                    where ca.nomCamping = 'Les Flots Bleus'
                    minus
                    select idClient
                    from LOCATIONS ll
                    join BUNGALOWS bb on ll.idBungalow = bb.idBungalow
                    where bb.superficieBungalow > 35
                    );

-- R133
select c.nomClient, c.prenomClient
from CLIENTS c
where villeClient in(
    select c1.villeCamping
    from CAMPINGS c1
    join CAMPINGS c2 on c1.idCamping != c2.idCamping and c1.villeCamping = c2.villeCamping
    );

-- R134

-- R135
select b.nomBungalow, ca.nomCamping
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping
where b.idBungalow in(
    select l.idBungalow
    from LOCATIONS l
    join LOCATIONS l2 on l.idBungalow = l2.idBungalow and l.idLocation != l2.idLocation
    where l.dateDebut >= '01/06/2021' and l.dateFin <= '30/06/2021' and l2.dateDebut >= '01/06/2021' and l2.dateFin <= '30/06/2021'
    );

-- R136
select nomCamping
from CAMPINGS ca
join EMPLOYES e on e.idCamping = ca.idCamping
group by(ca.nomCamping, ca.idCamping)
having avg(e.salaireEmploye) = (select min(avg(e.salaireEmploye))
                            from EMPLOYES e
                            group by(e.idCamping)
                            );

-- R138
select nomBungalow
from BUNGALOWS b1
where not exists(select categorieService
                 from SERVICES s
                 join PROPOSER p on p.idService = s.idService
                 join BUNGALOWS b on b.idBungalow = p.idBungalow
                 where nomBungalow='La Suite Régalienne'
                 minus
                 select categorieService
                 from SERVICES s
                 join PROPOSER p on s.idService = p.idService
                 where p.idBungalow = b1.idBungalow)
and not exists(select categorieService
               from SERVICES s
                        join PROPOSER p on s.idService = p.idService
               where p.idBungalow = b1.idBungalow
               minus
    select categorieService
    from SERVICES s
    join PROPOSER p on p.idService = s.idService
    join BUNGALOWS b on b.idBungalow = p.idBungalow
    where nomBungalow='La Suite Régalienne'
               )




-- 130 131 135 136 138 pour l'éval
