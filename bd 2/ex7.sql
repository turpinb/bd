-- Ex7
-- R100
select nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
group by nomBungalow, b.idBungalow
having count(p.idService) = (select count(idService)
                           from SERVICES);
-- bis
select nomBungalow
from BUNGALOWS b
where not exists(select idService
                 from SERVICES
                 minus
                 select idService
                 from PROPOSER p
                 where p.idBungalow = b.idBungalow);

-- R101
select nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
join SERVICES s on p.idService = s.idService
where s.categorieService = 'Luxe'
group by nomBungalow, b.idBungalow
having count(p.idService) = (select count(idService)
                             from SERVICES s
                             where s.categorieService = 'Luxe');
-- bis
select nomBungalow
from BUNGALOWS b
where not exists(select idService
                 from SERVICES s
                 where s.categorieService = 'Luxe'
                 minus
                 select idService
                 from PROPOSER p
                 where b.idBungalow = p.idBungalow);

-- R102
select nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
where idService in (select idService
                    from PROPOSER p
                    join BUNGALOWS b on b.idBungalow = p.idBungalow
                    where b.nomBungalow = 'La Poubelle')
group by nomBungalow, b.idBungalow
having count(p.idService) = (select count(idService)
                             from PROPOSER p
                             join BUNGALOWS b on b.idBungalow = p.idBungalow
                             where b.nomBungalow = 'La Poubelle');
-- bis
select nomBungalow
from BUNGALOWS b
where not exists(select idService
                 from PROPOSER p
                 join BUNGALOWS b on p.idBungalow = b.idBungalow
                 where b.nomBungalow = 'La Poubelle'
                 minus
                 select idService
                 from PROPOSER p
                 where b.idBungalow = p.idBungalow);

-- R103
select nomClient
from CLIENTS c
group by nomClient, idClient
having count(villeClient) = (select count(villeCamping)
                             from CAMPINGS);

-- R104
select nomClient
from CLIENTS c
where not exists(select villeCamping
                 from CAMPINGS
                 minus
                 select ca.villeCamping
                 from LOCATIONS l
                 join BUNGALOWS b on b.idBungalow = l.idBungalow
                 join CAMPINGS ca on b.idCamping = ca.idCamping
                 where l.idClient = c.idClient);

-- R105
select nomClient, prenomClient
from CLIENTS c1
where not exists(select idCamping
                 from BUNGALOWS b
                 join LOCATIONS l on b.idBungalow = l.idBungalow
                 join CLIENTS aga on aga.idClient = l.idClient
                 where aga.nomClient = 'Zeblouse' and aga.prenomClient = 'Agathe'
                 minus
                 select ca.idCamping
                 from LOCATIONS l
                 join BUNGALOWS b on b.idBungalow = l.idBungalow
                 join CAMPINGS ca on ca.idCamping = b.idCamping
                 where c1.idClient = l.idClient)
  and not exists((select ca.idCamping
                  from LOCATIONS l
                           join BUNGALOWS b on b.idBungalow = l.idBungalow
                           join CAMPINGS ca on ca.idCamping = b.idCamping
                  where c1.idClient = l.idClient
                      minus
                  select idCamping
                  from BUNGALOWS b
                      join LOCATIONS l
                  on b.idBungalow = l.idBungalow
                      join CLIENTS aga on aga.idClient = l.idClient
                  where aga.nomClient = 'Zeblouse' and aga.prenomClient = 'Agathe'));

-- Ex8
-- R110
select nomService
from SERVICES s
where not exists(select idBungalow
                 from BUNGALOWS b
                 where b.superficieBungalow > 60
                 minus
                 select idBungalow
                 from PROPOSER s1
                 where s1.idService = s.idService);

-- R111
select c.idClient, c.nomClient, c.prenomClient
from CLIENTS c
where not exists(select idLocation
                 from LOCATIONS l
                 join BUNGALOWS b on l.idBungalow = b.idBungalow
                 join CAMPINGS ca on ca.idCamping = b.idCamping
                 where ca.villeCamping = 'Palavas'
                 and c.idClient = l.idClient)
order by c.nomClient;

-- R112
select nomService
from SERVICES s
join PROPOSER p on s.idService = p.idService
join BUNGALOWS b on b.idBungalow = p.idBungalow
join CAMPINGS ca on ca.idCamping = b.idCamping
where ca.nomCamping = 'Les Flots Bleus'
and b.superficieBungalow = (select max(superficieBungalow)
                            from BUNGALOWS b
                            join CAMPINGS ca on ca.idCamping = b.idCamping
                            where ca.nomCamping = 'Les Flots Bleus');

-- R113
select e.nomEmploye, e.prenomEmploye, count(sub.idEmploye) as NB_SUB
from EMPLOYES e
join CAMPINGS ca on e.idCamping = ca.idCamping
left join EMPLOYES sub on sub.idEmployeChef = e.idEmploye
where ca.nomCamping = 'La Décharge Monochrome'
group by e.nomEmploye, e.prenomEmploye;

-- R114
select nomCamping
from CAMPINGS c
where not exists(select *
                 from BUNGALOWS b
                 where superficieBungalow <=50
                 and b.idCamping = c.idCamping);

-- R115
select nomClient
from CLIENTS
where idClient in(select idClient
                  from LOCATIONS
                  having count(idLocation) = (select count(idLocation)
                                             from LOCATIONS l
                                             join CLIENTS c on c.idClient = l.idClient
                                             where c.nomClient = 'Zeblouse' and c.prenomClient = 'Agathe')
                  group by (idClient)
                  );

-- R116

select nomService
from SERVICES s
join PROPOSER p on s.idService = p.idService