-- ex1
-- R1
select nomClient, prenomClient
from CLIENTS
where villeClient = 'Montpellier'
order by nomClient;

-- R2
select distinct c.idClient, nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on l.idClient = c.idClient
where montantLocation > 1000;
-- alt
select nomClient, prenomClient
from CLIENTS
where idClient in (select idClient
                   from LOCATIONS
                   where montantLocation > 1000);

-- R3
select distinct c.idClient, nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
join PROPOSER p on l.idBungalow = p.idBungalow
join SERVICES s on s.idService = p.idService
where nomService = 'Climatisation' and villeClient = 'Montpellier';
-- alt
select idClient, nomClient, prenomClient
from CLIENTS
where villeClient = 'Montpellier'
and idClient in (select idClient
                 from LOCATIONS
                 where idBungalow in (select idBungalow
                                      from PROPOSER
                                      where idService in (select idService
                                                          from SERVICES
                                                          where nomService = 'Climatisation')));

-- R4
select ch.nomEmploye, ch.prenomEmploye
from EMPLOYES e
join EMPLOYES ch on ch.idEmploye = e.idEmployeChef
where e.nomEmploye = 'Deuf' and e.prenomEmploye = 'John';
-- alt
select nomEmploye, prenomEmploye
from EMPLOYES
where idEmploye in(select idEmployeChef
                   from EMPLOYES
                   where nomEmploye = 'Deuf' and prenomEmploye = 'John');

-- R5
select count(b.idBungalow) as Nombre_bungalows
from BUNGALOWS b
join CAMPINGS c on b.idCamping = c.idCamping
where nomCamping = 'Les Flots Bleus';
-- alt
select count(idBungalow) as Nombre_bungalows
from BUNGALOWS
where idCamping in (select idCamping
                    from CAMPINGS
                    where nomCamping = 'Les Flots Bleus');

-- R6
select avg(superficieBungalow) as Moyenne_superficie_bungalows
from BUNGALOWS b
join CAMPINGS c on b.idCamping = c.idCamping
where nomCamping = 'Les Flots Bleus';
-- alt
select avg(superficieBungalow) as Moyenne_superficie_bungalows
from BUNGALOWS
where idCamping in (select idCamping
                    from CAMPINGS
                    where nomCamping = 'Les Flots Bleus');

-- R7
select count(distinct categorieService) as Nombre_categorie
from SERVICES;

-- R8
select nomEmploye
from EMPLOYES
where idEmploye in(select idEmploye
                   from EMPLOYES
                   minus
                   select em.idEmploye
                   from EMPLOYES e
                   join EMPLOYES em on em.salaireEmploye < e.salaireEmploye);

-- alt
select nomEmploye
from EMPLOYES
where salaireEmploye = (select max(salaireEmploye)
                        from EMPLOYES);

-- R9
select nomClient
from CLIENTS
where idClient in(select idClient
                  from CLIENTS
                  minus
                  select idClient
                  from LOCATIONS);
-- alt
select nomClient
from CLIENTS
where idClient not in(select idClient
                      from LOCATIONS);

-- R10
(select b.nomBungalow, distinct idCamping
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
join SERVICES s on s.idService = p.idService
where nomService = 'Climatisation')
intersect
(select b.nomBungalow, distinct idCamping
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
join SERVICES s on s.idService = p.idService
where nomService = 'TV');
-- alt
(select nomBungalow, idCamping
 from BUNGALOWS
 where idBungalow in(select idBungalow
                     from PROPOSER
                     where idService in(select idService
                                        from SERVICES
                                        where nomService = 'Climatisation')))
intersect
(select nomBungalow, idCamping
 from BUNGALOWS
 where idBungalow in(select idBungalow
                     from PROPOSER
                     where idService in(select idService
                                        from SERVICES
                                        where nomService = 'Climatisation')));

-- ex2
-- R11
select nomEmploye, prenomEmploye
from EMPLOYES
where idCamping in(select idCamping
                   from CAMPINGS
                   where nomCamping = 'Les Flots Bleus')
order by salaireEmploye desc;

-- R12
select distinct c.idClient, nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
join BUNGALOWS b on b.idBungalow = l.idBungalow
join CAMPINGS ca on ca.idCamping = b.idCamping
where ca.villeCamping = 'Palavas'
and dateNaissanceClient < '01/01/2000';

-- R13
select nomClient
from CLIENTS
where idClient in(select idClient
                  from LOCATIONS
                  where idBungalow in(select idBungalow
                                      FROM BUNGALOWS
                                      where superficieBungalow < 40))
order by nomClient;

-- R14
select nomClient, prenomClient
from CLIENTS
where idClient in(select c.idClient
                  from CLIENTS c
                  join CAMPINGS ca on c.villeClient = ca.villeCamping);

-- R15
select e.nomEmploye, e.prenomEmploye
from EMPLOYES e
join EMPLOYES e_g on e.idEmployeChef = e_g.idEmploye
where e_g.nomEmploye = 'Alizan' and e_g.prenomEmploye = 'Gaspard';
-- bis
select nomEmploye, prenomEmploye
from EMPLOYES
where idEmployeChef in( select idEmploye
                        from EMPLOYES
                        where nomEmploye = 'Alizan' and prenomEmploye = 'Gaspard');

-- R16
select c.idClient, nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
join BUNGALOWS b on b.idBungalow = l.idBungalow
join CAMPINGS ca on ca.idCamping = b.idCamping
where ca.nomCamping = 'Les Flots Bleus'
and l.dateDebut < '14/07/2021' and l.dateFin > '14/07/2021';

-- R17
(select nomClient, prenomClient
from CLIENTS
where idClient in(select idClient
                  from LOCATIONS
                  where dateDebut >= '01/07/2021' and dateFin <= '31/07/2021'
                  and idBungalow in(select idBungalow
                                      from BUNGALOWS
                                      where idCamping in(select idCamping
                                                         from CAMPINGS
                                                         where nomCamping = 'Les Flots Bleus'))
                  UNION
                  select idClient
                  from LOCATIONS
                  where dateDebut <= '01/07/2021' and dateFin >= '31/07/2021'
                    and idBungalow in(select idBungalow
                                      from BUNGALOWS
                                      where idCamping in(select idCamping
                                                         from CAMPINGS
                                                         where nomCamping = 'Les Flots Bleus'))
                  ))
UNION
(select nomClient, prenomClient
 from CLIENTS
 where idClient in(select idClient
from LOCATIONS
where dateDebut >= '01/07/2021' and dateFin >= '31/07/2021' and dateDebut <= '31/07/2021'
  and idBungalow in(select idBungalow
                    from BUNGALOWS
                    where idCamping in(select idCamping
                                       from CAMPINGS
                                       where nomCamping = 'Les Flots Bleus'))
    ))
UNION
(select nomClient, prenomClient
 from CLIENTS
 where idClient in(select idClient
from LOCATIONS
where dateDebut <= '01/07/2021' and dateFin <= '31/07/2021' and dateFin >= '01/07/2021'
  and idBungalow in(select idBungalow
                    from BUNGALOWS
                    where idCamping in(select idCamping
                                       from CAMPINGS
                                       where nomCamping = 'Les Flots Bleus'))));

-- R18
select count(idService) as NombreService
from BUNGALOWS b
join PROPOSER p on p.idBungalow = b.idBungalow
where nomBungalow = 'Le Titanic';

-- R19
select max(salaireEmploye) as SalaireMAX
from EMPLOYES e
join CAMPINGS c on c.idCamping = e.idCamping
where nomCamping = 'Les Flots Bleus';

-- R20
select count(DISTINCT ca.idCamping)
from CAMPINGS ca
         join BUNGALOWS b on b.idCamping = ca.idCamping
         join LOCATIONS l on l.idBungalow = b.idBungalow
         join CLIENTS c on c.idClient = l.idClient
where c.nomClient = 'Zeblouse' and c.prenomClient = 'Agathe';

-- R21
select nomBungalow
from BUNGALOWS
where superficieBungalow in (select max(superficieBungalow)
                     from BUNGALOWS);

-- R22
select nomEmploye, prenomEmploye
from EMPLOYES e
         join CAMPINGS c on c.idCamping = e.idCamping
where nomCamping = 'Les Flots Bleus'
and salaireEmploye = (select min(salaireEmploye)
                      from EMPLOYES e
                      join CAMPINGS c on c.idCamping = e.idCamping
                      where nomCamping = 'Les Flots Bleus');

-- R23
select nomBungalow
from BUNGALOWS
where idBungalow in(select idBungalow
                    from BUNGALOWS
                    minus
                    select idBungalow
                    from PROPOSER);

-- R24
SELECT nomEmploye, prenomEmploye
FROM EMPLOYES e1
WHERE NOT EXISTS (SELECT *
                  FROM EMPLOYES e2
                  WHERE e2.idEmployeChef = e1.idEmploye);
-- bis
select nomEmploye, prenomEmploye
from EMPLOYES
where idEmploye in(select idEmploye
                   from EMPLOYES
                   minus
                   select idEmployeChef
                   from EMPLOYES);

-- R25
select b.idBungalow, nomBungalow
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping
where nomCamping = 'La Décharge Monochrome'
union
select b.idBungalow, nomBungalow
from BUNGALOWS b
join PROPOSER p on p.idBungalow = b.idBungalow
join SERVICES s on p.idService = s.idService
where nomService = 'Kit de Bain'

-- R26
select nomBungalow
from BUNGALOWS
where idBungalow in(select idBungalow
                    from BUNGALOWS
                    minus
                    ((select idBungalow
                    from PROPOSER p
                    join SERVICES s on p.idService = s.idService
                    where nomService = 'Climatisation')
                    union
                    (select idBungalow
                    from PROPOSER p
                             join SERVICES s on p.idService = s.idService
                    where nomService = 'TV')));
-- bis
select nomBungalow
from BUNGALOWS
where idBungalow not in (
    ((select idBungalow
      from PROPOSER p
               join SERVICES s on p.idService = s.idService
      where nomService = 'Climatisation')
     union
     (select idBungalow
      from PROPOSER p
               join SERVICES s on p.idService = s.idService
      where nomService = 'TV')));

-- R27a
select nomClient, prenomClient
from CLIENTS
where idClient in (select idClient
                   from LOCATIONS l
                            join BUNGALOWS b on b.idBungalow = l.idBungalow
                            join CAMPINGS ca on ca.idCamping = b.idCamping
                   where nomCamping = 'Les Flots Bleus'
                   intersect
                   select idClient
                   from LOCATIONS l
                            join BUNGALOWS b on b.idBungalow = l.idBungalow
                            join CAMPINGS ca on ca.idCamping = b.idCamping
                   where nomCamping = 'La Décharge Monochrome')
order by nomClient, prenomClient;

-- R27b
select nomClient, prenomClient
from CLIENTS
where idClient in (select idClient
                   from LOCATIONS l
                            join BUNGALOWS b on b.idBungalow = l.idBungalow
                            join CAMPINGS ca on ca.idCamping = b.idCamping
                   where nomCamping = 'Les Flots Bleus'
                   union
                   select idClient
                   from LOCATIONS l
                            join BUNGALOWS b on b.idBungalow = l.idBungalow
                            join CAMPINGS ca on ca.idCamping = b.idCamping
                   where nomCamping = 'La Décharge Monochrome')
order by nomClient, prenomClient;

-- R28
select nomEmploye, prenomEmploye, NVL(nomCamping, 'Pas affecté à un camping') as NOMCAMPING
from EMPLOYES e
left join CAMPINGS ca on e.idCamping = ca.idCamping
order by nomEmploye;

-- R29
select nomClient, prenomClient
from ClIENTS
where idClient in(select l.idCLient
                  from LOCATIONS l
                  join BUNGALOWS b on b.idBungalow = l.idBungalow
                  join CAMPINGS ca on ca.idCamping = b.idCamping
                  join CAMPINGS ca_jb on ca_jb.idCamping = ca.idCamping
                  join BUNGALOWS b_jb on b_jb.idCamping = ca_jb.idCamping
                  join LOCATIONS l_jb on l_jb.idBungalow = b_jb.idBungalow
                  join CLIENTS jb on jb.idClient = l_jb.idClient
                  where jb.nomClient = 'Bricot' and jb.prenomClient = 'Judas'
                    AND (
                          (l.dateDebut <= l_jb.dateDebut AND l.dateFin >= l_jb.dateDebut)
                          OR (l.dateDebut <= l_jb.dateFin AND l.dateFin >= l_jb.dateFin)
                          OR (l.dateDebut >= l_jb.dateDebut AND l.dateFin <= l_jb.dateFin)
                      )
                  minus
                  select idClient
                  from CLIENTS
                  where nomClient = 'Bricot' and prenomClient = 'Judas');

-- Ex 3 & 4
-- R3A
select nomEmploye, prenomEmploye
from EMPLOYES e
where not EXISTS(select idEmploye
                 from EMPLOYES ch
                 where e.idEmployeChef = ch.idEmploye);
-- R3B
select nomBungalow
from BUNGALOWS
where idBungalow not in(select idBungalow
                        from LOCATIONS);
-- bis
select nomBungalow
from BUNGALOWS
where idBungalow in(select b.idBungalow
                    from BUNGALOWS b
                    minus
                    select l.idBungalow
                    from LOCATIONS l);
-- bis
select nomBungalow
from BUNGALOWS b
where not exists(select *
                 from LOCATIONS l
                 where b.idBungalow = l.idBungalow);

-- R30
select nomCamping
from CAMPINGS c
where not exists(select *
                 from EMPLOYES e
                 where c.idCamping = e.idCamping);

-- R31
select count(idBungalow)
From BUNGALOWS b
where not exists (select *
                  from PROPOSER p
                  where b.idBungalow = p.idBungalow);

-- R32
select nomClient
from CLIENTS
where idClient in(select idClient
                  from LOCATIONS
                  minus
                  select idClient
                  from LOCATIONS l
                  join BUNGALOWS b on b.idBungalow = l.idBungalow
                  where b.superficieBungalow < 58);
-- bis
select nomClient
from CLIENTS c1
where not exists(select *
                 from LOCATIONS l2
                 join BUNGALOWS b2 on b2.idBungalow = l2.idBungalow
                 where superficieBungalow < 58
                 and l2.idClient = c1.idClient)
and exists (select *
            from LOCATIONS l3
            where l3.idClient = c1.idClient);

-- R33
select nomCamping
from CAMPINGS
where idCamping in (select c.idCamping
                    from CAMPINGS c
                    join EMPLOYES e on e.idCamping = c.idCamping
                    minus
                    select c1.idCamping
                    from CAMPINGS c1
                    join EMPLOYES e1 on e1.idCamping = c1.idCamping
                    where e1.salaireEmploye < 1000);

-- R34
select nomClient
from CLIENTS
where villeClient = 'Montpellier'
and idClient not in (select c.idClient
                 from CLIENTS c
                 join LOCATIONS l on c.idClient = l.idClient
                 where l.idBungalow in(select idBungalow
                                       from BUNGALOWS
                                       minus
                                       select idBungalow
                                       from PROPOSER));

-- R35
select nomClient, prenomClient
from CLIENTS
where idClient not in(select idClient
                      from LOCATIONS);
-- bis
select nomClient, prenomClient
from CLIENTS
where idClient in(select idClient
                  from CLIENTS
                  MINUS
                  select idClient
                  from LOCATIONS);
-- bis
select nomClient, prenomClient
from CLIENTS c
where not exists(select idClient
                 from LOCATIONS l
                 where c.idClient = l.idClient);

-- R36
select nomCamping
from CAMPINGS
where idCamping in(select idCamping
                   from CAMPINGS
                   minus
                   select idCamping
                   from LOCATIONS l
                   join BUNGALOWS b on l.idBungalow = b.idBungalow
                   where b.superficieBungalow > 50);

-- R37
select count(idClient)
from CLIENTS c
join LOCATIONS lo on c.idClient = lo.idClient
where not exists(select idClient
                 from LOCATIONS l
                 where c.idClient = l.idClient
                 and montantLocation < 990);

-- R38
select nomClient
from CLIENTS c
where idCLient not in(select c.idClient
                      from CLIENTS c
                      join CAMPINGS ca on c.villeClient = ca.villeCamping)
and c.prenomCLient like 'J%';

-- R39
select categorieService
from SERVICES
minus
select categorieService
from SERVICES s
join PROPOSER p on s.idService = p.idService
join BUNGALOWS b on b.idBungalow = p.idBungalow
join CAMPINGS ca on b.idCamping = ca.idCamping
where ca.nomCamping = 'La Décharge Monochrome';

-- R40
select distinct c.idClient, nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
join BUNGALOWS b on b.idBungalow = l.idBungalow
join CAMPINGS ca on b.idCamping = ca.idCamping
where c.villeClient = ca.villeCamping;

-- R41
select nomClient, prenomClient
from CLIENTS
where idClient not in (select idClient
                       from LOCATIONS l
                       join BUNGALOWS b on b.idBungalow = l.idBungalow
                       join CAMPINGS c on c.idCamping = b.idCamping
                       where c.nomCamping = 'Les Flots Bleus');

-- R42
select count(idBungalow) as NB_BUNGALOWS
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping

-- R50
select nomEmploye
from EMPLOYES
where idCamping is null;

-- R51
select e.nomEmploye, e.prenomEmploye
from EMPLOYES e
where idEmploye in(select idEmployeChef
                   from EMPLOYES e1
                   where e1.idEmployeChef = e.idEmploye
                   and e1.idEmploye in(select idEmployeChef
                                       from EMPLOYES e2
                                       where e2.idEmployeChef = e1.idEmploye));

-- R52
select nomService
from SERVICES s
where idService in(select idService
                   from SERVICES
                   minus
                   select idService
                   from PROPOSER p
                   join BUNGALOWS b on b.idBungalow = p.idBungalow
                   where b.superficieBungalow > 60);

-- R53
select nomCamping
from CAMPINGS
where villeCamping = 'Palavas'
and nbEtoilesCamping =(select max(nbEtoilesCamping)
                       from CAMPINGS
                       where villeCamping = 'Palavas');

-- R54
select nomBungalow
from BUNGALOWS b
join PROPOSER p on p.idBungalow = b.idBungalow
join SERVICES s on s.idService = p.idService
where s.nomService = 'Chaine Hi-Fi'
intersect
select nomBungalow
from BUNGALOWS b
         join PROPOSER p on p.idBungalow = b.idBungalow
         join SERVICES s on s.idService = p.idService
where s.nomService = 'Climatisation';

-- R55
-- R55
select nomService
from SERVICES
where categorieService = 'Loisir'
   or idService not in(select idService
                       from PROPOSER p
                                join BUNGALOWS b on b.idBungalow = p.idBungalow
                                join CAMPINGS ca on b.idCamping = ca.idCamping
                       where ca.nomCamping = 'The White Majestic');

-- R56
select nomClient
from CLIENTS c
where idClient in(select idClient
                  from LOCATIONS l
                  join BUNGALOWS b on l.idBungalow = b.idBungalow
                  join CAMPINGS ca on b.idCamping = ca.idCamping
                  where ca.nomCamping = 'La Décharge Monochrome');

-- R57
select nomBungalow
from BUNGALOWS
where idBungalow in(select idBungalow
                    from BUNGALOWS
                    minus
                    select idBungalow
                    from LOCATIONS)
and superficieBungalow = (select min(superficieBungalow)
                          from BUNGALOWS
                          where idBungalow in(select idBungalow
                                              from BUNGALOWS
                                              minus
                                              select idBungalow
                                              from LOCATIONS));

-- R58
select nomCamping
from CAMPINGS c
where not exists(select idbungalow
                 from BUNGALOWS b
                 where c.idCamping = b.idCamping
                 and not exists(select idService
                                  from PROPOSER p
                                  where p.idBungalow = b.idBungalow));

-- R59
select nomBungalow, nomService
from BUNGALOWS b
cross join SERVICES sc
where idCamping = 'CAMP1'
minus
select ba.nomBungalow, nomService
from BUNGALOWS ba
join PROPOSER pa on ba.idBungalow = pa.idBungalow
join SERVICES sa on pa.idBungalow = sa.idService
where sa.idService in(select idService
                   from SERVICES
                   where categorieService = 'Luxe')
and ba.idCamping = 'CAMP1';