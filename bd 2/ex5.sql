-- ex5
-- R6A
select categorieService, count(*)
from SERVICES
group by categorieService;

-- R6B
select villeClient
from CLIENTS
group by(villeClient)
having count(*) > 2;

-- R6C
select nomCamping, avg(salaireEmploye)
from CAMPINGS ca
join EMPLOYES e on ca.idCamping = e.idCamping
group by (nomCamping);

-- R6D
select nomCamping
from EMPLOYES e
join CAMPINGS ca on ca.idCamping = e.idCamping
group by nomCamping
having count(idEmploye) > 3;

-- R60
select nomClient, prenomClient, count(*) as NB_LOCATION
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
group by(nomClient, prenomClient, c.idClient)
order by count(l.idLocation) desc;

-- R61
select nomCamping
from CAMPINGS ca
join EMPLOYES e on ca.idCamping = e.idCamping
group by(nomCamping)
having avg(e.salaireEmploye) > 1400;

-- R62
select nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on l.idClient = c.idClient
join BUNGALOWS b on b.idBungalow = l.idBungalow
group by(nomClient, prenomClient, c.idClient)
having count(distinct b.idCamping) = 2;

-- R63
select nomBungalow, count(p.idService)
from BUNGALOWS b
left join PROPOSER p on p.idBungalow = b.idBungalow
group by(nomBungalow, b.idBungalow)
order by count(p.idService) desc;

-- R64
select nomCamping
from CAMPINGS c
join BUNGALOWS b on c.idCamping = b.idCamping
where b.superficieBungalow < 65
group by (nomCamping)
order by count(b.idBungalow);

-- R65
select nomCamping
from CAMPINGS ca
join EMPLOYES e on e.idCamping = ca.idCamping
group by(nomCamping)
having min(e.salaireEmploye) >= 1000;

-- R66
select nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
group by nomBungalow
having count(*) = (select count(*)
                   from BUNGALOWS br
                   join PROPOSER pr on br.idBungalow = pr.idBungalow
                   where br.nomBungalow = 'Le Royal');

-- R67
select nomBungalow, count(l.idLocation)
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping
left join LOCATIONS l on b.idBungalow = l.idBungalow
where ca.nomCamping = 'La Décharge Monochrome'
group by(nomBungalow, b.idBungalow)
order by count(l.idLocation) desc;

-- R68
select nomClient, prenomClient
from CLIENTS c
join LOCATIONS l on c.idClient = l.idClient
group by(nomClient, prenomClient, c.idClient)
having avg(l.montantLocation) > 1100 and count(l.idLocation) > 1;

-- R69
select nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
group by (nombungalow, b.idBungalow)
having count(p.idService) = (
    select max(count(*))
    from PROPOSER p
    group by(p.idBungalow)
    );

-- ex6

-- R70
select villeCamping, count(idCamping)
from CAMPINGS
group by(villeCamping);

-- R71
select nomCamping
from EMPLOYES e
         join CAMPINGS ca on ca.idCamping = e.idCamping
group by nomCamping
having count(idEmploye) > 3;

-- R72
select nomCamping, count(idEmploye)
from CAMPINGS ca
left join EMPLOYES e on ca.idCamping = e.idCamping
group by nomCamping
order by count(idEmploye) desc;

-- R73
select nomService, count(distinct b.idCamping)
from SERVICES s
join PROPOSER p on p.idService = s.idService
join BUNGALOWS b on b.idBungalow = p.idBungalow
where categorieService = 'Luxe'
group by (nomService);

-- R74
select nomClient, prenomClient
from CLIENTS c
         join LOCATIONS l on c.idClient = l.idClient
group by (nomClient,prenomClient, c.idClient)
having count(l.idLocation) = (
    select max(count(*))
    from LOCATIONS l
    group by(idClient)
);

-- R80
select distinct nomBungalow
from BUNGALOWS b
join PROPOSER p on b.idBungalow = p.idBungalow
join LOCATIONS l on b.idBungalow = l.idBungalow
join SERVICES s on p.idService = s.idService
where s.nomService = 'Kit de Bain';

-- R81
select nomBungalow
from BUNGALOWS b
join LOCATIONS l on b.idBungalow = l.idBungalow
group by(nomBungalow, b.idBungalow)
having count(l.idLocation) > 4;

-- R82
select count(c.idClient) as Nb_client
from CLIENTS c
where idClient in(select idClient
                  from CLIENTS
                  minus
                  select idClient
                  from CLIENTS c
                  join CAMPINGS ca on ca.villeCamping = c.villeClient);

-- R83
select nomBungalow, count(p.idService)
from BUNGALOWS b
left join PROPOSER p on p.idBungalow = b.idBungalow
join CAMPINGS ca on b.idCamping = ca.idCamping
where ca.nomCamping = 'La Décharge Monochrome'
group by(nomBungalow, b.idBungalow);

-- R84
select nomBungalow
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping
where ca.nomCamping = 'Les Flots Bleus'
and superficieBungalow = (select min(superficieBungalow)
                          from BUNGALOWS b
                          join CAMPINGS ca on ca.idCamping = b.idCamping
                          where ca.nomCamping = 'Les Flots Bleus');

-- R85
select nomBungalow
from BUNGALOWS
where idBungalow in (select b.idBungalow
                     from BUNGALOWS b
                              join PROPOSER p on p.idBungalow = b.idBungalow
                     group by(b.idBungalow)
                     having count(p.idService) > 1
                     intersect
                     select b.idBungalow
                     from BUNGALOWS b
                              join LOCATIONS l on l.idBungalow = b.idBungalow
                     group by(b.idBungalow)
                     having count(l.idClient) > 2 );

-- R86 ne marche pas
select nomBungalow
from BUNGALOWS
where idBungalow in(
    select idBungalow
    from BUNGALOWS
    minus
    select idBungalow
    from LOCATIONS
    where (dateFin < '01/08/2021')
    or (dateDebut > '31/08/2021')
        );

-- R87
select e.nomEmploye, e.prenomEmploye
from EMPLOYES e
where idEmploye in(select idEmployeChef
                   from EMPLOYES e1
                   where e1.idEmployeChef = e.idEmploye
                   group by(idEmployeChef)
                   having count(idEmployeChef) > 1);