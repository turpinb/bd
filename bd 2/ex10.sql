-- 1
create or replace view BUNGALOWSLFB as
select b.idBungalow, b.nomBungalow, b.superficieBungalow
from BUNGALOWS b
join CAMPINGS c on b.idCamping = c.idCamping
where c.nomCamping = 'Les Flots Bleus';

select count(*)
from BUNGALOWSLFB;

-- 2
create or replace view LOCATIONSLFB as
select idLocation, l.idClient, nomClient, prenomClient, lfb.idBungalow, nomBungalow
from BUNGALOWSLFB lfb
join LOCATIONS l on l.idBungalow = lfb.idBungalow
join CLIENTS c on l.idClient = c.idCLient;

select idBungalow, nomBungalow, count(idBungalow) as NBLOCATIONS
from LOCATIONSLFB
group by(idBungalow, nomBungalow);

-- 3
create or replace view LOCATIONSBUNGALOWS as
select b.idBungalow, nomBungalow, b.idCamping, nomCamping,
       superficieBungalow, count(idLocation) as NBLOCATIONS, nvl(sum(montantLocation),0) as CA, sum(montantLocation)/count(idLocation) as CAML
from BUNGALOWS b
left join LOCATIONS l on b.idBungalow = l.idBungalow
join CAMPINGS ca on b.idCamping = ca.idCamping
group by (b.idBungalow, nomBungalow, b.idCamping, nomCamping, superficieBungalow)
order by idCamping, b.idBungalow;

-- 4
create or replace view SERVICESBUNGALOWS as
select b.idBungalow, nomBungalow, b.idCamping, nomCamping,
superficieBungalow, count(s.idService) as nbServices, count(distinct s.categorieService) as nbCategoriesServ
from BUNGALOWS b
join CAMPINGS ca on b.idCamping = ca.idCamping
left join PROPOSER p on p.idBungalow = b.idBungalow
left join SERVICES s on s.idService = p.idService
group by (b.idBungalow, nomBungalow, b.idCamping, nomCamping,
          superficieBungalow);

SELECT * FROM ServicesBungalows
WHERE superficieBungalow < 40;

-- 5
create or replace view DONNEESBUNGALOWS as
    select lb.idBungalow, lb.nomBungalow, lb.idCamping, lb.nomCamping, lb.superficieBungalow, NBLOCATIONS, CA, CAMl, nbServices, nbCategoriesServ
    from LOCATIONSBUNGALOWS lb
    join SERVICESBUNGALOWS sb on sb.idBungalow = lb.idBungalow;

select nomBungalow, superficieBungalow
from DONNEESBUNGALOWS db
where nbServices > 2 and nbLocations = 0;

select nomBungalow
from DONNEESBUNGALOWS db
where NBLOCATIONS = (select(max(NBLOCATIONS))
                    from DONNEESBUNGALOWS)
group by(idBungalow, nomBungalow);

select nomBungalow
from DONNEESBUNGALOWS db
where not exists(select idService
                 from SERVICES
                 minus
                 select p.idService
                 from PROPOSER p
                 where p.idBungalow = db.idBungalow);

-- 6
-- vue inter nombre Bungalow
create or replace view nbBungalowCamping as
    select ca.idCamping, count(distinct b.idBungalow) as nbBungalows, avg(b.superficieBungalow) as superficieMoyenneBungalows, sum (db.NBLOCATIONS) as nbloc, sum(db.CA) as CAA, round(sum(db.CA)/sum(db.NBLOCATIONS)) as CAMLL
    from CAMPINGS ca
    join BUNGALOWS b on ca.idCamping = b.idCamping
    join DONNEESBUNGALOWS db on db.idBungalow = b.idBungalow
    group by(ca.idCamping);

create or replace view SERVICES_CAMP as
    select b.idCamping, count(distinct p.idService) as nbserv, count(distinct s.categorieService) as nbcat
    from BUNGALOWS b
    join PROPOSER p on p.idBungalow = b.idBungalow
    join SERVICES s on s.idService = p.idService
    group by(b.idCamping);

-- vue finale
create or replace view DONNEESCAMPING as
    select ca.idCamping, ca.nomCamping, ca.villeCamping, ca.nbEtoilesCamping, nbc.nbBungalows, nbc.superficieMoyenneBungalows,
         nbc.nbloc, nbc.CAA, nbc.CAMLL, sc.nbserv, sc.nbcat
    from CAMPINGS ca
    left join nbBungalowCamping nbc on ca.idCamping = nbc.idCamping
    left join SERVICES_CAMP sc on sc.idCamping = ca.idCamping;

-- 8
create or replace view EmployesSansCamping as
select idEmploye, nomEmploye, prenomEmploye, salaireEmploye, idEmployeChef
from EMPLOYES e
where not exists(select idCamping
                 from CAMPINGS ca
                 where ca.idCamping = e.idCamping);

-- logiquement tout est possible, la clé primaire est présente dans la vue

INSERT INTO EmployesSansCamping values ('E100', 'Stiko','Judas',3000,null);

UPDATE EmployesSansCamping
set nomEmploye = 'Nana'
where idEmploye = 'E100';

DELETE from EmployesSansCamping
where idEmploye = 'E100';

-- 10
create or replace view ClientsParVille as
select c.villeClient, count(*) as nb_clients
from CLIENTS c
group by(villeClient);

select *
from ClientsParVille