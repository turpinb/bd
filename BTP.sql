DROP TABLE Regions CASCADE CONSTRAINT;
DROP TABLE Ouvriers CASCADE CONSTRAINT;
DROP TABLE Chantiers CASCADE CONSTRAINT;
DROP TABLE Travailler CASCADE CONSTRAINT;
DROP TABLE Absences CASCADE CONSTRAINT;

-- creation des tables

CREATE TABLE Regions
(nomRegion VARCHAR(30), Superficie NUMBER,
CONSTRAINT pk_Regions PRIMARY KEY (nomRegion)) ;

CREATE TABLE Ouvriers
(codeOuvrier VARCHAR(5), nomOuvrier VARCHAR(25), prenomOuvrier VARCHAR(25), salaire NUMBER, nomRegion VARCHAR(30),
CONSTRAINT pk_Ouvriers PRIMARY KEY (codeOuvrier),
CONSTRAINT fk_Ouvriers_nomRegion FOREIGN KEY (nomRegion) REFERENCES Regions(nomRegion)) ;

CREATE TABLE Chantiers
(numChantier VARCHAR(5), nomChantier VARCHAR(50), nomRegion VARCHAR(30), codeOuvrierChef VARCHAR(5),
CONSTRAINT pk_Chantiers PRIMARY KEY (numChantier),
CONSTRAINT fk_Chantiers_nomRegion FOREIGN KEY (nomRegion) REFERENCES Regions(nomRegion),
CONSTRAINT fk_Chantiers_codeOuvrierChef FOREIGN KEY (codeOuvrierChef) REFERENCES Ouvriers(codeOuvrier)) ;

CREATE TABLE Travailler
(codeOuvrier VARCHAR(13), numChantier VARCHAR(5), nbJoursTravail NUMBER,
CONSTRAINT pk_Travailler PRIMARY KEY (codeOuvrier, numChantier),
CONSTRAINT fk_Travailler_codeOuvrier FOREIGN KEY (codeOuvrier) REFERENCES Ouvriers(codeOuvrier),
CONSTRAINT fk_Travailler_numChantier FOREIGN KEY (numChantier) REFERENCES Chantiers(numChantier));

CREATE TABLE Absences
(codeOuvrier VARCHAR(13), dateAbsence DATE, motif VARCHAR(50),
CONSTRAINT pk_Absences PRIMARY KEY (codeOuvrier, dateAbsence),
CONSTRAINT fk_Absences_codeOuvrier FOREIGN KEY (codeOuvrier) REFERENCES Ouvriers(codeOuvrier));


--insertion de donn�es dans les tables

INSERT INTO Regions (nomRegion, superficie) (SELECT * FROM Pallejan.BTP_Regions);
INSERT INTO Ouvriers (codeOuvrier, nomOuvrier, prenomOuvrier, salaire, nomRegion) (SELECT * FROM Pallejan.BTP_Ouvriers);
INSERT INTO Chantiers (numChantier, nomChantier, nomRegion, codeOuvrierChef) (SELECT * FROM Pallejan.BTP_Chantiers);
INSERT INTO Travailler (codeOuvrier, numChantier, nbJoursTravail) (SELECT * FROM Pallejan.BTP_Travailler);
INSERT INTO Absences (codeOuvrier, dateAbsence, motif) (SELECT * FROM Pallejan.BTP_Absences);

COMMIT;
