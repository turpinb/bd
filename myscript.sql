-- R1
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A1'
UNION
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A5'

-- R2
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A1'
intersect
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A5'

--R3
(SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A1'
UNION
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A5')
MINUS
(SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A1'
intersect
SELECT nomLivre
FROM Livres l
join Ecrire e on l.numLivre = e.numLivre
where e.numAuteur = 'A5')

-- R4
select nomEditeur
from Editeurs
MINUS
select nomEditeur
from Livres
where nomEditeur IS NOT NULL

-- R5
select nomEditeur
from Livres
where sujet='Informatique'
intersect
select nomEditeur
from Livres
where sujet='Economie'

-- R6
select nomLivre
from Livres
where nomEditeur = 'Nathan'
UNION
select nomLivre
from Livres l
join Ecrire e On l.numLivre = e.numLivre
join Auteurs a On e.numAuteur = a.numAuteur
where nomAuteur = 'Oracle' AND prenomAuteur = 'Escuelle'

-- R7
select nomEditeur
from Livres
MINUS
select nomEditeur
from Livres
where sujet = 'Informatique'

-- R8
select nomEditeur
from Livres
where nomEditeur IS NOT NULL
MINUS
select nomEditeur
from Livres
where sujet <> 'Informatique'

-- R9
SELECT nomAuteur, prenomAuteur
from Auteurs
Where numAuteur IN (
  select numAuteur
  from Ecrire
)

-- R10
Select nomLivre
from Livres
where numLivre IN (
  select l.numLivre
  from Livres l
  join Auteurs a on  a.anneeNaissance = l.anneeEdition
)

-- R11
select nomEditeur, anneeEdition
from Livres
where numLivre in (
  select numLivre
  from Ecrire e
  where e.numAuteur = 'A4'
)
order by anneeEdition DESC

-- R12
select nomLivre
from Livres
where numLivre in (
  select l.numLivre
  from livres l
  join Ecrire e On l.numLivre = e.numLivre
  join Auteurs a On e.numAuteur = a.numAuteur
  Where l.prix < 10 AND a.nomAuteur = 'Nateur'
  AND a.prenomAuteur = 'Jordi'
)

-- R13
select nomLivre, prix
from livres
where numLivre in (
  (select l.numLivre
  FROM livres l
  join livres l_ab on l.prix > l_ab.prix
  where l_ab.nomLivre = 'Access pour les balaises')
)
order by prix;

-- R14
SELECT nomAuteur, prenomAuteur
FROM Auteurs
Where numAuteur IN (SELECT numAuteur
                    FROM Auteurs

                    MINUS

                    SELECT numAuteur
                    FROM Ecrire)
-- R15
SELECT nomAuteur, prenomAuteur
FROM Auteurs
Where numAuteur IN (
  select numAuteur
  from Ecrire
  MINUS
  select numAuteur
  from Ecrire e
  join livres l on e.numLivre = l.numLivre
  where sujet <> 'Informatique'
)

-- R16
select nomAuteur, prenomAuteur
from Auteurs
where numAuteur in (
  (select numAuteur
  from Ecrire e
  join Livres l on e.numLivre = l.numLivre
  where l.nomEditeur = 'Nathan'
  UNION
  select numAuteur
  from Ecrire e
  join Livres l on e.numLivre = l.numLivre
  where l.nomEditeur = 'PC Poche')
  MINUS
  (select numAuteur
  from Ecrire e
  join Livres l on e.numLivre = l.numLivre
  where l.nomEditeur = 'Nathan'
  intersect
  select numAuteur
  from Ecrire e
  join Livres l on e.numLivre = l.numLivre
  where l.nomEditeur = 'PC Poche')
)
