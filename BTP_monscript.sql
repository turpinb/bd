-- R1
Select *
From Regions
Where nomRegion like '%o%'
Order by nomRegion;

-- R2
Select *
From Ouvriers
Where (salaire > 1500 AND nomRegion = 'Occitanie') or
(salaire < 1500 AND nomRegion <> 'Occitanie')
order by salaire desc;

-- R3
select c.nomChantier, c.nomRegion, superficie
from Chantiers c
join Regions r on c.nomRegion = r.nomRegion
Where r.superficie > 40000
order by r.superficie;

-- R4
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (Select codeOuvrierChef
                      from Chantiers
                      where numChantier = 'C1');

-- R5
select o.codeOuvrier, nomOuvrier, prenomOuvrier, motif
from Ouvriers o
join Absences a on a.codeOuvrier = o.codeOuvrier
where a.dateAbsence = '01/07/22';

-- R6
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (select codeOuvrier
                      from Travailler)
order by nomOuvrier;

-- R7
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (select codeOuvrier
                      from Ouvriers
                      minus
                      select codeOuvrier
                      from Travailler);

-- R8
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (select codeOuvrier
                      from Travailler
                      where numChantier = 'C1' or numChantier = 'C2');

-- R9
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (select codeOuvrier
                      from Travailler
                      where numChantier = 'C1'
                      intersect
                      select codeOuvrier
                      From Travailler
                      where numChantier = 'C2');

-- R10
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in ((select codeOuvrier
                      from Travailler
                      where numChantier = 'C1'
                      union
                      select codeOuvrier
                      From Travailler
                      where numChantier = 'C2')
                      minus
                      (select codeOuvrier
                      from Travailler
                      where numChantier = 'C1'
                      intersect
                      select codeOuvrier
                      From Travailler
                      where numChantier = 'C2'));

-- R11
select nomChantier
from Chantiers c
join Travailler t on c.numChantier = t.numChantier
join Ouvriers o on o.codeOuvrier = t.codeOuvrier
where o.nomOuvrier = 'Bricot' and o.prenomOuvrier = 'Judas'

-- R12
select codeOuvrier,nomOuvrier,prenomOuvrier, numChantier
from Ouvriers
cross join Chantiers
where Ouvriers.nomRegion = 'PACA'
minus
select t.codeOuvrier,nomOuvrier,prenomOuvrier, numChantier
from Travailler t
join Ouvriers o on t.codeOuvrier = o.codeOuvrier;

-- R13
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (
  select codeOuvrierChef
  from Chantiers
  where codeOuvrierChef is not null
);

-- R14
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (
  select codeOuvrier
  from Ouvriers
  minus
  select codeOuvrier
  from Absences
  where dateAbsence like '%/07/22'
);

-- R15
select nomChantier
from Chantiers
minus
select nomChantier
from Chantiers
where codeOuvrierChef is not null;

-- R16
select distinct t1.numChantier
from Travailler t1
join Travailler t2 on t1.numChantier = t2.numChantier and t1.codeOuvrier <> t2.codeOuvrier;

-- R17
select o.codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers o
join Travailler t on o.codeOuvrier = t.codeOuvrier
where t.nbJoursTravail > 80;

-- R18
select nomChantier
from Chantiers
minus
select nomChantier
from Chantiers c
join Travailler t on c.numChantier = t.numChantier
where t.codeOuvrier in(
  select codeOuvrier
  from Ouvriers
  where salaire <= 1500
);

-- R19
select nomOuvrier, prenomOuvrier, salaire
from Ouvriers
where codeOuvrier in(
  select o1.codeOuvrier
  from Ouvriers o1
  join Ouvriers o_JD on o1.salaire > o_JD.salaire
  where o_JD.nomOuvrier = 'Bricot' and o_JD.prenomOuvrier = 'Judas'
  )
order by salaire;

-- R20
select nomOuvrier, prenomOuvrier
from Ouvriers o
where codeOuvrier in (
  select codeOuvrier
  from Ouvriers o
  join Chantiers c on c.nomRegion = o.nomRegion
)
order by nomOuvrier;


-- R21
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (
  select codeOuvrier
  from Ouvriers
  where nomRegion = 'Occitanie'
  union
  select codeOuvrier
  from Travailler t
  join Chantiers c on t.numChantier = c.numChantier
  where c.nomRegion = 'Occitanie'
);

-- R22
select t.nbJoursTravail
from Travailler t
join Ouvriers o on o.codeOuvrier = t.codeOuvrier
where o.nomOuvrier = 'Bricot' and o.prenomOuvrier = 'Judas' and numChantier = 'C1';

-- R23
select o.codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers o
join Travailler t on t.codeOuvrier = o.codeOuvrier
join Chantiers c on c.numChantier = t.numChantier
where o.nomRegion <> c.nomRegion
order by nomOuvrier;

-- R24
select numChantier
from Travailler t
join Ouvriers o on t.codeOuvrier = o.codeOuvrier
where o.nomOuvrier = 'Bricot' and o.prenomOuvrier = 'Judas'
intersect
select numChantier
from Travailler t
join Ouvriers o on t.codeOuvrier = o.codeOuvrier
where o.nomOuvrier = 'Nanas' and o.prenomOuvrier = 'Judas';

-- R25
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in(
  select o.codeOuvrier
  from Ouvriers o
  join Travailler t on o.codeOuvrier = t.codeOuvrier
  where o.codeOuvrier != 'O6' and t.numChantier in(
    select t.numChantier
    from Travailler t
    where t.codeOuvrier = 'O6'
  )
)

-- R26
select nomRegion
from Ouvriers
minus
Select nomRegion
from Chantiers
where nomRegion is not null;

-- R27
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers
minus
select codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers o
join  Chantiers c on o.codeOuvrier = c.codeOuvrierChef;

-- R28
select o.codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers o
join Travailler t on o.codeOuvrier = t.codeOuvrier
minus
select o.codeOuvrier, nomOuvrier, prenomOuvrier
from Ouvriers o
join Travailler t on o.codeOuvrier = t.codeOuvrier
join Chantiers c on t.numChantier = c.numChantier
where c.nomRegion <> o.nomRegion;

-- R29
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in (
    select t1.codeOuvrier
    from Travailler t1
    join Travailler t2 on t1.codeOuvrier = t2.codeOuvrier and t1.numChantier <> t2.numChantier

    join Chantiers c1 on t1.numChantier = c1.numChantier
    join Chantiers c2 on t2.numChantier = c2.numChantier
    where c1.nomRegion = 'Occitanie' and c2.nomRegion = 'Occitanie'
    );

-- R30 marche pas
select nomOuvrier, prenomOuvrier
from Ouvriers o
join Travailler t on o.codeOuvrier = t.codeOuvrier
join Chantiers c on t.numChantier = c.numChantier
where codeOuvrier in (
    select t.codeOuvrier
    from Travailler t
    join Chantiers c on t.numChantier = t.numChantier
    MINUS
    select t1.codeOuvrier
    from Travailler t1
    join Travailler t2 on t1.codeOuvrier = t2.codeOuvrier and t1.numChantier <> t2.numChantier
    )
and c.nomRegion = 'Occitanie';

-- R31
select nomOuvrier, prenomOuvrier
from Ouvriers
where codeOuvrier in(
    select c1.codeOuvrierChef
    from Chantiers c1
    join Chantiers c2 on c1.codeOuvrierChef = c2.codeOuvrierChef and c1.numChantier <> c2.numChantier
    );

-- R32
select nomRegion
from Regions
MINUS
select r1.nomRegion
from Regions r1
join Regions r2 on r1.superficie < r2.superficie;

-- R33
select nomOuvrier, prenomOuvrier
from Ouvriers
MINUS
select o1.nomOuvrier, o1.prenomOuvrier
from Ouvriers o1
join Ouvriers o2 on o1.salaire > o2.salaire;
